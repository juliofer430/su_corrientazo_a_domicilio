package corrientazo.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import corrientazo.vo.Drone;

@Testable
public class CorrientazoServiceTest {

	@InjectMocks
	private CorrientazoService service;
	
	@BeforeEach
	private void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void launchDronesSuccess() {
		List<Drone> drones = service.launchDrones();
		assertNotNull(drones);
	}
	
}
