package corrientazo.validation;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import corrientazo.enumeration.DroneOrientation;
import corrientazo.generator.CorrientazoGenerator;
import corrientazo.service.DroneRuleProcessorService;

public class CorrientazoValidationTest {

	@InjectMocks
	private DroneRuleProcessorService validator;
	
	@BeforeEach
	private void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void validateMoveSuccess() {
		CorrientazoGenerator gen = new CorrientazoGenerator();
		assertEquals(DroneOrientation.NORTH, validator.processMove(gen.generateDrone(), "A").getOrientation());
	}
}
