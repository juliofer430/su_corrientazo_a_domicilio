package corrientazo.generator;

import java.util.ArrayList;
import java.util.Arrays;

import corrientazo.enumeration.DroneOrientation;
import corrientazo.vo.DeliverHistoric;
import corrientazo.vo.Drone;
import corrientazo.vo.DronePosition;

public class CorrientazoGenerator {

	public Drone generateDrone() {
		Drone drone = new Drone();
		drone.setCurrentPosition(new DronePosition(0, 0));
		drone.setLunchesAmmount(3);
		drone.setOrientation(DroneOrientation.NORTH);
		drone.setDroneFile("");
		drone.setDroneIdentifier("01");
		drone.setInstructions(Arrays.asList("AAAAIAA"));
		drone.setHistoric(new ArrayList<DeliverHistoric>());
		return drone;
	}
	
}
