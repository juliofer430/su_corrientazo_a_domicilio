package corrientazo.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import corrientazo.enumeration.CorrientazoProperties;

public class FileUtil {
	
	private PropertiesLoader properties;
	
	public FileUtil() {
		properties = PropertiesLoader.getInstance();
	}

	public List<String> detectInputFiles(){
		
		File folder = new File(properties.getProperty(CorrientazoProperties.INPUT_FILE.getValue()));
		File[] listOfFiles = folder.listFiles();
		List<String> files = new ArrayList<String>();
		
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				if (listOfFiles[i].getName().startsWith("in")){
					files.add(listOfFiles[i].getName());
				}	
			} 
		}
		
		return files;
	}
	
	public ArrayList<String> readFiles(String fileName) throws IOException{
		
		Path pathToRead = Paths.get(properties.getProperty(CorrientazoProperties.INPUT_FILE.getValue()) + fileName);
		if(!Files.exists(pathToRead)) {
			   System.out.println("File does not exist");
			   return null;
		};
		
		ArrayList<String> instructions = new ArrayList<String>();
		
		Files.lines(pathToRead).forEach(line -> {
			instructions.add(line);
		});
		
		return instructions;
	}
	
	public void writeFiles(List<String> droneInfo, String droneId) throws IOException{
		
		Path pathToWrite = Paths.get(properties.getProperty(CorrientazoProperties.OUTPUT_FILE.getValue()) + "out"  + droneId + ".txt");
		Files.write(pathToWrite, droneInfo);
	}
	
	public String obtainFileId(String fileName) {
		return fileName.replace("in", "").replace(".txt", "");
	}
	
}
