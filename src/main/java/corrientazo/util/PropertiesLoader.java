package corrientazo.util;
import java.io.IOException;
import java.util.Properties;

public class PropertiesLoader {
	 
	private static PropertiesLoader instancia=null;
	private Properties p;
	
	private PropertiesLoader() {
 
		p = new Properties();
		try {
			p.load(PropertiesLoader.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
	public static PropertiesLoader getInstance() {
 
		if (instancia==null) {
 
			instancia=new PropertiesLoader();
		}
		return instancia;
	}
 
	public String getProperty(String clave) {
 
		return p.getProperty(clave);
	}
}
