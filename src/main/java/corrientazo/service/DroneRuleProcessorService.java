package corrientazo.service;

import corrientazo.enumeration.CorrientazoProperties;
import corrientazo.enumeration.DroneMove;
import corrientazo.enumeration.DroneOrientation;
import corrientazo.exception.NeighborhoodException;
import corrientazo.util.PropertiesLoader;
import corrientazo.vo.Drone;
import corrientazo.vo.DronePosition;
import corrientazo.vo.Neighborhood;

public class DroneRuleProcessorService {

	private PropertiesLoader properties;
	
	private Neighborhood neighborhood;
	
	public DroneRuleProcessorService() {
		this.neighborhood = this.loadNeighborhood();
	}
	
	public Drone processMove(Drone drone, String move) throws NeighborhoodException{
	
		if (move.contentEquals(DroneMove.D.getValue())) {
			if (drone.getOrientation().equals(DroneOrientation.NORTH)){
				drone.setOrientation(DroneOrientation.EAST);
			} else if (drone.getOrientation().equals(DroneOrientation.EAST)){
				drone.setOrientation(DroneOrientation.SOUTH);
			} else if (drone.getOrientation().equals(DroneOrientation.SOUTH)){
				drone.setOrientation(DroneOrientation.WEST);
			} else if (drone.getOrientation().equals(DroneOrientation.WEST)){
				drone.setOrientation(DroneOrientation.NORTH);
			}
		} else if (move.contentEquals(DroneMove.I.getValue())) {
			if (drone.getOrientation().equals(DroneOrientation.NORTH)){
				drone.setOrientation(DroneOrientation.WEST);
			} else if (drone.getOrientation().equals(DroneOrientation.WEST)){
				drone.setOrientation(DroneOrientation.SOUTH);
			} else if (drone.getOrientation().equals(DroneOrientation.SOUTH)){
				drone.setOrientation(DroneOrientation.EAST);
			} else if (drone.getOrientation().equals(DroneOrientation.EAST)){
				drone.setOrientation(DroneOrientation.NORTH);
			}
		}
		
		if (move.equals(DroneMove.A.getValue())){
			DronePosition position =  drone.getCurrentPosition();
			
			int toGo;
			if (drone.getOrientation().equals(DroneOrientation.NORTH)){
				toGo = position.getYPosition() + 1;
				this.validateNeighborhoodLimit(toGo, neighborhood.getMinY(), neighborhood.getMaxY());
				position.setYPosition(toGo);
			} else if (drone.getOrientation().equals(DroneOrientation.SOUTH)){
				toGo = position.getYPosition() - 1;
				this.validateNeighborhoodLimit(toGo, neighborhood.getMinY(), neighborhood.getMaxY());
				position.setYPosition(toGo);
			} else if (drone.getOrientation().equals(DroneOrientation.EAST)){
				toGo = position.getXPosition() + 1;
				this.validateNeighborhoodLimit(toGo, neighborhood.getMinX(), neighborhood.getMaxX());
				position.setXPosition(toGo);
			} else if (drone.getOrientation().equals(DroneOrientation.WEST)){
				toGo = position.getXPosition() - 1;
				this.validateNeighborhoodLimit(toGo, neighborhood.getMinX(), neighborhood.getMaxX());
				position.setXPosition(position.getXPosition() - 1);
			}
			
			drone.setCurrentPosition(position);
		}
		
		
		System.out.println("MOVE " + move + " - " + drone.getCurrentPosition().toString() + " " + drone.getOrientation().getValue());
		return drone;
	}
	
	private Neighborhood loadNeighborhood() {
		int maxBlocksAround = Integer.parseInt(properties.getInstance().getProperty(CorrientazoProperties.MAX_BLOCKS_AROUND.getValue())) / 2;
		return new Neighborhood(maxBlocksAround * (-1), maxBlocksAround * (-1), maxBlocksAround, maxBlocksAround);
	}
	
	private void validateNeighborhoodLimit(int toGoValue, int minLimit, int maxLimit) throws NeighborhoodException{
		if (toGoValue < minLimit || toGoValue > maxLimit) {
			throw new NeighborhoodException("The neighborhood limit cannot be surpased");
		}
	}
}
