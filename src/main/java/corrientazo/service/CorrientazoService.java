package corrientazo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import corrientazo.enumeration.CorrientazoProperties;
import corrientazo.enumeration.DroneOrientation;
import corrientazo.util.FileUtil;
import corrientazo.util.PropertiesLoader;
import corrientazo.vo.DeliverHistoric;
import corrientazo.vo.Drone;
import corrientazo.vo.DronePosition;

public class CorrientazoService {

	private FileUtil fileUtil;
	
	private DroneRuleProcessorService validator;
	
	private PropertiesLoader properties;
	
	public CorrientazoService(){
		fileUtil = new FileUtil();
		validator = new DroneRuleProcessorService();
		
	}
	
	public List<Drone> launchDrones() {
		List<Drone> drones = new ArrayList<Drone>();
		
		try{
			
			List<String> inputFiles = fileUtil.detectInputFiles();
			for (String in : inputFiles) {
				Drone drone = this.createNewDrone(in, fileUtil.readFiles(in));
				drones.add(drone);
			}
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}	
		
		this.processDrones(drones);
		
		this.generateOutputFile(drones);
		
		return drones;
	}
	
	private void processDrones(List<Drone> drones) {
	
		
		for (Drone drone : drones) {
			 for (String instruction : drone.getInstructions()) { 
				 
				 System.out.println("DRONE: " + drone.getDroneIdentifier() + " - Instruction : " + instruction);
				 
				 for (int i = 0; i < instruction.length(); i++) { 
					 drone = validator.processMove(drone, String.valueOf(instruction.charAt(i)));
				 }
				 
				 DeliverHistoric dh = new DeliverHistoric();
				 dh.setOrientation(drone.getOrientation());
				 DronePosition dp = new DronePosition(drone.getCurrentPosition().getXPosition(), drone.getCurrentPosition().getYPosition());
				 dh.setPosition(dp);
				 drone.getHistoric().add(dh);
			 }
		}
	}
	
	private void generateOutputFile(List<Drone> drones) {
		try{
			for (Drone drone : drones) {
				List<String> outputFile = new ArrayList<String>();
				for (DeliverHistoric dh : drone.getHistoric()) {
					String line = "(" + dh.getPosition().getXPosition() + ", " + dh.getPosition().getYPosition() + ") " + dh.getOrientation().getValue();
					outputFile.add(line);
				}
				fileUtil.writeFiles(outputFile, drone.getDroneIdentifier());
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}	
		
		
	}

	
	private Drone createNewDrone(String fileName, List<String> instructions) {
		Drone drone = new Drone();
		drone.setCurrentPosition(new DronePosition(0, 0));
		drone.setLunchesAmmount(Integer.parseInt(properties.getInstance().getProperty(CorrientazoProperties.LUNCH_NUMBER.getValue())));
		drone.setOrientation(DroneOrientation.NORTH);
		drone.setDroneFile(fileName);
		drone.setDroneIdentifier(fileUtil.obtainFileId(fileName));
		drone.setInstructions(instructions);
		drone.setHistoric(new ArrayList<DeliverHistoric>());
		return drone;
	}
	
}
