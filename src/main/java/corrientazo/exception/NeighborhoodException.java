package corrientazo.exception;

public class NeighborhoodException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4234038878014925873L;

	public NeighborhoodException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
	
	public NeighborhoodException(String errorMessage) {
        super(errorMessage);
    }
}
