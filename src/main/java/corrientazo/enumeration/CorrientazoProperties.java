package corrientazo.enumeration;

public enum CorrientazoProperties {

	LUNCH_NUMBER("cor.maxLunchNumber"),
	MAX_BLOCKS_AROUND("cor.maxBlocksAround"),
	INPUT_FILE("cor.inputFilePath"),
	OUTPUT_FILE("cor.outputFilePath");
				
	private String value;
	
	CorrientazoProperties(String value){
		this.value = value;
	}
	
	public String getValue(){
	    return value;
	}
			
}
