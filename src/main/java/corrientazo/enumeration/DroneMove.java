package corrientazo.enumeration;

public enum DroneMove {

	A("A"),
	I("I"),
	D("D");
	
	private String value;
	
	DroneMove(String value){
		this.value = value;
	}
	
	public String getValue(){
        return value;
    }
}
