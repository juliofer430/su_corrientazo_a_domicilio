package corrientazo.enumeration;

public enum DroneOrientation {
	
	NORTH("North"),
	SOUTH("South"),
	EAST("East"),
	WEST("West");
	
	private String value;
	
	DroneOrientation(String value){
		this.value = value;
	}
	
	public String getValue(){
        return value;
	}
	
}
