package corrientazo.vo;

import corrientazo.enumeration.DroneOrientation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DeliverHistoric {

	private DronePosition position;
	
	private DroneOrientation orientation;
}
