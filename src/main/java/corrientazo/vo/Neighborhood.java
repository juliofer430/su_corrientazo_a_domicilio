package corrientazo.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Neighborhood implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -7454965487668449376L;
	
	private int minX, minY, maxX, maxY;
 
 
}
