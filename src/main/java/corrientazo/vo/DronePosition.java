package corrientazo.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DronePosition implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7955432384395106512L;
	
	private int xPosition, yPosition;
}
