package corrientazo.vo;

import java.io.Serializable;
import java.util.List;

import corrientazo.enumeration.DroneOrientation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Drone implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6355833262592409384L;
	
	private String droneIdentifier;
	
	private DronePosition currentPosition;
	
	private DroneOrientation orientation;
	
	private Integer lunchesAmmount;
	
	private String droneFile;
	
	private List<String> instructions;
	
	private List<DeliverHistoric> historic;
	
}
