package corrientazo.execution;

import corrientazo.service.CorrientazoService;

public class CorrientazoMain {

	public static void main(String[] args) {
		
		CorrientazoService service = new CorrientazoService();
		service.launchDrones();
		
	}
}
